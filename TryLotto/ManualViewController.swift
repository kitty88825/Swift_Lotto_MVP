//
//  ManualViewController.swift
//  TryLotto
///Users/ntubimdc/Desktop/TryLotto/TryLotto/AutoViewController.swift
//  Created by NTUBIMD on 2018/3/25.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import UIKit


class ManualViewController: UIViewController ,Interface{
    
    var delegate:Presenter!
    var prize = String()
    var betting = String()
    var winning = String()
    var confirm = Bool()
    
    @IBOutlet weak var text1: UITextField!
    @IBOutlet weak var text2: UITextField!
    @IBOutlet weak var text3: UITextField!
    @IBOutlet weak var text4: UITextField!
    @IBOutlet weak var text5: UITextField!
    @IBOutlet weak var text6: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBAction func enterButton(_ sender: UIButton) {
            self.prize = String(describing: self.delegate.prize())
            self.betting = String(describing:self.delegate.munualBetting(type1: self.text1.text!,  type2: self.text2.text!, type3: self.text3.text!, type4: self.text4.text!, type5: self.text5.text!, type6: self.text6.text!))
            self.winning = String(describing: self.delegate.winning())
        if confirm {
            self.performSegue(withIdentifier: "ManualToAuto", sender: nil)
        }
    }
    
// -------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.delegate = Presenter(interface: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let autoVC = segue.destination as! AutoViewController
            autoVC.prize = prize
            autoVC.betting = betting
            autoVC.winning = winning
        
    }
// -------------------------------------------------------------------
    func enter(confirm:Bool){
        if confirm{
//            self.performSegue(withIdentifier: "ManualToAuto", sender: nil)
            self.confirm = confirm
        }else{
            errorLabel.text = "輸入錯誤請重新輸入"
        }
    }
    
    func getNum() {
        
    }
// -------------------------------------------------------------------
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
// -------------------------------------------------------------------
    
}
