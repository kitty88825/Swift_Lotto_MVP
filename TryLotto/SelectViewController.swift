//
//  SelectViewController.swift
//  TryLotto
//
//  Created by NTUBIMD on 2018/3/25.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import UIKit

class SelectViewController: UIViewController,Interface{
    func enter(confirm: Bool) {
        print("")
    }
    
    
    var delegate : Presenter!
//    var prizeFT = Numbers(enterPC: self)

    
    @IBAction func autoButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectToAuto", sender: nil)
    }
    
    @IBAction func manualButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SelectToManual", sender: nil)
    }
    
// ----------------------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = Presenter(interface: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectToAuto"{
            let autoVC = segue.destination as! AutoViewController
            autoVC.prize = String(describing: self.delegate.prize())
            autoVC.betting = String(describing: self.delegate.autoBetting())
            autoVC.winning = String(describing: self.delegate.winning())
        }
        
    }
// ----------------------------------------------------------------------------
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

// ----------------------------------------------------------------------------
    
}
