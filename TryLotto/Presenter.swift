//
//  ManualModel.swift
//  TryLotto
//
//  Created by NTUBIMD on 2018/3/26.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import Foundation

protocol Delegate {
    func prize() -> Array<Int>
    func autoBetting() -> Array<Int>
    func winning() -> String
    func munualBetting(type1:String,type2:String,type3:String,type4:String,type5:String,type6:String) -> Array<String>
}

protocol Interface {
    func enter(confirm:Bool)
}

class Presenter: Delegate {
    
    var interface: Interface!
    
    var bingo = Array<Int>()
    var autoN = Array<Int>()
    var bingoTot = 0
    var typeO = Array<String>()
    var typeN = Array<String>()
    
    init(interface:Interface) {
        self.interface = interface
    }
    
    // 開獎號碼
    func prize() -> Array<Int>{
        while(bingo.count < 6){
            let randomNumber = Int(arc4random_uniform(49) + 1)
            if !bingo.contains(randomNumber){
                bingo.append(randomNumber)
            }
        }
        bingo.sort { $0 < $1 }
        return bingo
    }
    
    // auto號碼
    func autoBetting() -> Array<Int>{
        while(autoN.count < 6){
            let randomNumber0 = Int(arc4random_uniform(49) + 1)
            if !autoN.contains(randomNumber0){
                autoN.append(randomNumber0)
                
            }
        }
        autoN.sort { $0 < $1 }
        return autoN
    }
    
    // 中獎號碼
    func winning() -> String {
        var text = ""
        if autoN.count != 0{// 自動選號
            for k in 0...5{
                for kk in 0...5{
                    if bingo[k] == autoN[kk]{
                        bingoTot += 1
                        if bingoTot > 1{
                            text += " , " + String(autoN[kk])
                        }else{
                            text = String(autoN[kk])
                        }
                    }
                }
            }
            if bingoTot == 0{
                text = "未中獎"
            }
            return text
        }else{// 手動輸入
            if typeO.count == 6{
                for k in 0...5{
                    for kk in 0...5{
                        print(Int(typeO[kk])!)
                        if bingo[k] == Int(typeO[kk])!{
                            bingoTot += 1
                            if bingoTot > 1{
                                text += " , " + String(bingo[k])
                            }else{
                                text = String(bingo[k])
                            }
                        }
                    }
                }
                if bingoTot == 0{
                    text = "未中獎"
                }
            }
            return text
        }
    }
    
    // manual號碼
    func munualBetting(type1:String,type2:String,type3:String,type4:String,type5:String,type6:String) -> Array<String> {
        typeN.removeAll(keepingCapacity: true)
        typeO.removeAll(keepingCapacity: true)
        typeN.append(type1)
        typeN.append(type2)
        typeN.append(type3)
        typeN.append(type4)
        typeN.append(type5)
        typeN.append(type6)
        
        
        
        
        for i in 0...5{
            if Int(typeN[i])?.description.count == typeN[i].count && Double(typeN[i])?.description.count != typeN[i].count && Int(typeN[i])! <= 49 && Int(typeN[i])! > 0 && !typeO.contains(typeN[i]){
                typeO.append(typeN[i])
                if i == 5 && typeO.count == 6{
                    self.interface.enter(confirm: true)
                }
            }else{
                self.interface.enter(confirm: false)
            }
        }
        print(typeN)
        return typeO
    }
}
