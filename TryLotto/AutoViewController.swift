//
//  AutoViewController.swift
//  TryLotto
//
//  Created by NTUBIMD on 2018/3/25.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import UIKit

class AutoViewController: UIViewController {
    
// ---------------------------------------------------------------------
    
    var prize:String = ""
    var betting:String = ""
    var winning:String = ""
    
    @IBOutlet weak var prizeLabel: UILabel!
    @IBOutlet weak var bettingLabel: UILabel!
    @IBOutlet weak var winningLabel: UILabel!
    
    @IBAction func yesButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AutoToSelect", sender: nil)
    }
    
    @IBAction func noButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AutoToView", sender: nil)
    }
    
// ---------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        prizeLabel.text = prize
        bettingLabel.text = betting
        winningLabel.text = winning
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// ---------------------------------------------------------------------
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

// ---------------------------------------------------------------------
    
}












//向上跳轉
//self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

//向上跳轉到第一頁
//var rootVC = self.presentingViewController
//while let parent = rootVC?.presentingViewController{
//    rootVC = parent
//}
//rootVC?.dismiss(animated: true, completion: nil)


//        self.present(ViewController(), animated: true, completion: nil)
