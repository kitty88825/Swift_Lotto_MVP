//
//  ViewController.swift
//  TryLotto
//
//  Created by NTUBIMD on 2018/3/25.
//  Copyright © 2018年 NTUBIMD. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // 
    @IBAction func startButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ViewToSelect", sender: nil)
    }
    
// ----------------------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// ----------------------------------------------------------------------------

}

